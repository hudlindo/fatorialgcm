package Calculo;

public class Fatorial {

	public static void main(String[] args) {
		System.out.println(fatorial(4));
	}

	public static int fatorial(int numero) {

		int fat = 1;

		for (int i = 2; i <= numero; i++) {
			fat *= i;
		}
		return fat;
	}
}
