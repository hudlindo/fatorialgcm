package fatorialgcm.Testes;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class Testes {

	@Test
	public void FatorialComQuatro() {
		int esperado = 24;
		int valor = Calculo.Fatorial.fatorial(4);
		assertEquals(esperado, valor);
	}

	@Test
	public void FatorialComSeis() {
		int esperado = 720;
		int valor = Calculo.Fatorial.fatorial(6);
		assertEquals(esperado, valor);
	}

}
